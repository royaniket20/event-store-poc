package org.example;

import com.eventstore.dbclient.*;
import org.example.events.AccountCreated;

import java.io.IOException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

public class EventListner {

    public static void main(String[] args) throws InterruptedException, IOException, ExecutionException {

        EventStoreDBClientSettings setts = EventStoreDBConnectionString.parseOrThrow("esdb://admin:changeit@localhost:2113,localhost:2111,localhost:2112?tls=true&tlsVerifyCert=false&requireMaster=true");
        EventStoreDBClient client = EventStoreDBClient.create(setts);
//        ReadStreamOptions readStreamOptions = ReadStreamOptions.get()
//                .fromStart()
//                .notResolveLinkTos();
/*

        CompletableFuture<ReadResult> readResult = client
                .readStream("accounts", readStreamOptions);

        readResult.get().getEvents().forEach(item->{
                    try {
                        AccountCreated writtenEvent = item.getOriginalEvent()
                                .getEventDataAs(AccountCreated.class);
                        System.out.println("Resolved event ==========> "+writtenEvent);
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                });
*/

        SubscriptionListener listener = new SubscriptionListener() {
            @Override
            public void onEvent(Subscription subscription, ResolvedEvent event) {
                try {
                    System.out.println( event.getOriginalEvent().getRevision()+ "**************"+Thread.currentThread().toString());
                    System.out.println(subscription.getSubscriptionId()+"___"+event.getOriginalEvent().getEventDataAs(AccountCreated.class));
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        };
        client.subscribeToStream(
                "accounts",
                listener,
                SubscribeToStreamOptions.get().fromStart()
        );
        System.out.println("Main ----" + Thread.currentThread().toString());
        while (true);

        }

}
