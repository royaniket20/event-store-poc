package org.example;

import com.eventstore.dbclient.*;
import com.eventstore.dbclient.proto.persistentsubscriptions.PersistentSubscriptionsGrpc;
import org.example.events.AccountCreated;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

public class GroupEventListner {


    public static void main(String[] args) throws InterruptedException, IOException, ExecutionException {

        EventStoreDBClientSettings setts = EventStoreDBConnectionString.parseOrThrow("esdb://admin:changeit@localhost:2113,localhost:2111,localhost:2112?tls=true&tlsVerifyCert=false&requireMaster=true");
//        EventStoreDBClient client = EventStoreDBClient.create(setts);
        EventStoreDBPersistentSubscriptionsClient client = EventStoreDBPersistentSubscriptionsClient.create(setts);
        PersistentSubscriptionListener listener = new PersistentSubscriptionListener() {
            @Override
            public void onEvent(PersistentSubscription subscription, int retryCount, ResolvedEvent event) {
                try {
                    System.out.println( event.getOriginalEvent().getRevision()+ "*****GROUP MESSAGE*********"+Thread.currentThread().toString());
                    System.out.println(subscription.getSubscriptionId()+"___"+event.getOriginalEvent().getEventDataAs(AccountCreated.class));
                    subscription.ack(event);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        };
        client.subscribeToStream(
                "accounts",
                "accounts-group",
                listener
        );
        System.out.println("Main ----" + Thread.currentThread().toString());
        while (true);

        }

}
