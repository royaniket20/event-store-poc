package org.example;

import com.eventstore.dbclient.*;
import org.example.events.AccountCreated;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) {
        System.out.println("Hello World!");

        EventStoreDBClientSettings setts = EventStoreDBConnectionString.parseOrThrow("esdb://admin:changeit@localhost:2113,localhost:2112,localhost:2111?tls=true&tlsVerifyCert=false&requireMaster=true");
        EventStoreDBClient client = EventStoreDBClient.create(setts);
        try {
            System.out.println(client.getServerVersion().get());

            while (true) {
                AccountCreated createdEvent = new AccountCreated();
                 String id = UUID.randomUUID().toString();
                createdEvent.setId(UUID.fromString(id));
                createdEvent.setLogin("Aniket Roy _"+id);
                System.out.println("Generated Event Data ---- " + createdEvent);
                EventData event = EventData
                        .builderAsJson("account-created", createdEvent).metadataAsBytes("{\"name\" : \"my metadata\"}".getBytes(StandardCharsets.UTF_8))
                        .build();
                System.out.println("Generated Event  ---- " + event);
                CompletableFuture<WriteResult> accounts = client.appendToStream("accounts", event);
                    WriteResult writeResult =  accounts.get();
                    System.out.println("Written Result ====> " +writeResult);
              Thread.sleep(1500);
            }
            // Doing something productive...
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
